/*****************************************************************************

		Copyright (c) My Company

 Project:  ALMAZON
 FileName: ALMAZON.PRO
 Purpose: No description
 Written by: Visual Prolog
 Comments:
******************************************************************************/

include "almazon.inc"


domains
	nombre=symbol
	posicion=symbol
	objeto=symbol
	unidades=integer
	distancia=integer
	lineapedido=linea(objeto,unidades)
	pedido=lineapedido*
	operario=estado(posicion,pedido,inventario,distancia)
	lista=operario*
	inventario=lineapedido*
	limite=integer
	
predicates
	estanteria(nombre,posicion)
	conectado(posicion,posicion,distancia)
	miembro(operario,lista)
	ubicacion(objeto,posicion,unidades)
	mueve(operario,operario)
	append(inventario,inventario,inventario)
	delete(lineapedido,pedido,pedido)
	imprime(lista)
	resuelve(lista,operario,limite,limite)
	mejorsol(integer)
	/*convertirAobjetos(pedido,inventario,inventario)*/
	
clauses
	/*colocacion de las estanterias*/
	estanteria(s1,ps1).
	estanteria(s2,ps2).
	estanteria(s3,ps3).
	estanteria(s4,ps4).
	estanteria(s5,ps5).
	estanteria(s6,ps6).
	estanteria(s7,ps7).
	estanteria(s8,ps8).
	
	/*estanterias y cruces conectados*/
	conectado(ps1,pc1,5).
	conectado(ps5,pc1,5).
	conectado(pc1,ps1,5).
	conectado(pc1,ps5,5).	
	
	conectado(ps2,pc2,5).
	conectado(ps6,pc2,5).
	conectado(pc2,ps2,5).
	conectado(pc2,ps2,5).
	
	conectado(ps3,pc3,5).
	conectado(ps7,pc3,5).
	conectado(pc3,pc3,5).
	conectado(pc3,ps7,5).
	
	conectado(ps4,pc4,5).
	conectado(ps8,pc4,5).
	conectado(pc4,ps4,5).
	conectado(pc4,ps8,5).
	/*pasillos horizontales, cruces*/
	conectado(ps,pc1,10).
	conectado(pc1,pc2,10).
	conectado(pc2,pc3,10).
	conectado(pc3,pc4,10).
	conectado(pc4,pf,10).
	conectado(pc1,ps,10).
	conectado(pc2,pc1,10).
	conectado(pc3,pc2,10).
	conectado(pc4,pc3,10).
	conectado(pf,pc4,10).
	
	/*DB alamacen objetos*/
	/*estanteria 1*/
	ubicacion(patatas,ps1,200).
	ubicacion(melones,ps1,100).
	ubicacion(patatas,ps1,200).
	ubicacion(melones,ps1,100).
	/*estanteria 2*/
	ubicacion(boligrafos,ps2,500).
	/*estanteria 3*/
	ubicacion(boligrafos,ps3,400).
	ubicacion(colonias,ps3,150).
	/*estanteria 4*/
	ubicacion(ratones,ps4,210).
	ubicacion(berzas,ps4,100).
	ubicacion(melocotones,ps4,200).
	/*estanteria 5*/
	ubicacion(papeles,ps5,500).
	/*estanteria 6*/
	ubicacion(boligrafos,ps6,400).
	/*estanteria 7*/
	ubicacion(plumas,ps7,500).
	/*estanteria 8*/
	ubicacion(plumas,ps8,400).

	/*Coger*/
	mueve(estado(PI,P,InvIni,D),estado(PI,PF,InvFinal,DF)):-DF=D,P=[LP|T],LP=linea(O,C),ubicacion(O,PI,_),estanteria(_,PI),append(InvIni,[LP],InvFinal),PF=T.
	/*movimiento*/
	mueve(estado(PI,P,I,DI),estado(PF,P,I,DF)):-conectado(PI,PF,D),DF=DI+D.
	
	append([], L, L).
	append([H|T], L, [H|R]) :-append(T, L, R).
	
	delete(X,[X|T],T).
	delete(X,[H|T],[H|NT]):-
		delete(X,T,NT).
	
	/*Estados repetidos */
        miembro(E,[E|_]).
        miembro(E,[_|T]):-
        	miembro(E,T).
       	/*extraer los objetos del pedido*/
       	/*convertirAobjetos([A],B,C):- A= linea(O,Q),
       		B=[H|T],C=[O].*/
       		
        /*Resolucion de algoritmo */
        resuelve(Lista,Destino,_,_):-
        	Lista=[H|T],
        	Destino=H,
        	imprime(Lista).
        
        resuelve(Lista,Destino,Lim_ant,Limite):-
        	Lista=[H|T],
        	not(miembro(H,T)),
        	mueve(H,Hfinal),
        	Nlista=[Hfinal|Lista],
        	Nue_Lim=Lim_ant+1,
        	Nue_Lim<=Limite,
        	resuelve(Nlista,Destino,Nue_Lim,Limite).
        	
        /*Escritura de la lista */
        imprime([]).
        imprime([H|T]):-
        	imprime(T),
        	write(H,'\n').
        	
        mejorsol(Lim_ini):-
        	/*resuelve([estado(s,[PLUMA])],estado(k,[PLUMA]),1,Lim_ini).*/
        	resuelve([estado(ps,[linea(patatas,40),linea(boligrafos,40),linea(plumas,40)],[],0)],
        	estado(pf,[],_,_),1,Lim_ini).
        	/*resuelve([estado(ps,[],[])],estado(pf,[],[]),1,Lim_ini).*/
        mejorsol(Lim_ini):-
        	Nue_lim=Lim_ini+1,
        	mejorsol(Nue_lim).

goal
	mejorsol(1).
	/*linea(patatas,40),linea(boligrafos,40),linea(plumas,10).*/